package INF102.lab6.cheapFlights;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {

        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();

        for (Flight flight : flights) {
            graph.addVertex(flight.start);
            graph.addVertex(flight.destination);
            graph.addEdge(flight.start, flight.destination, flight.cost);
        }

        return graph;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {

        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        PriorityQueue<Path> pq = new PriorityQueue<>(Comparator.comparingInt(path -> path.cost));
        Map<City, Map<Integer, Integer>> cheapestCosts = new HashMap<>();

        pq.offer(new Path(start, 0, -1)); 

        while (!pq.isEmpty()) {
            Path current = pq.poll();
            if (current.stops == nMaxStops && !current.city.equals(destination)) {
                continue;
            }

            if (current.city.equals(destination)) {
                return current.cost;
            }

            for (City neighbor : graph.outNeighbours(current.city)) {
                int newCost = current.cost + graph.getWeight(current.city, neighbor);
                
                if (newCost < 0) {
                    continue;
                }

                if (!cheapestCosts.containsKey(neighbor) || 
                    !cheapestCosts.get(neighbor).containsKey(current.stops + 1) ||
                    cheapestCosts.get(neighbor).get(current.stops + 1) > newCost) {
                    cheapestCosts.putIfAbsent(neighbor, new HashMap<>());
                    cheapestCosts.get(neighbor).put(current.stops + 1, newCost);
                    pq.offer(new Path(neighbor, newCost, current.stops + 1));
                }
            }
        }

        return Integer.MAX_VALUE;
    }

    private static class Path {
        City city; 
        int cost; 
        int stops; 

        public Path(City city, int cost, int stops) {
            this.city = city;
            this.cost = cost;
            this.stops = stops;
        }
    }
}